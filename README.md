# Design Docs

## Visão geral do projeto

Desenvolvimento de uma API que traga a funcionalidades gerenciamento de clientes e de lista de
produtos favoritos.

### Contexto

Atualmente não existe nenhuma API que fornece
essa funcionalidade de wishlist para os nossos aplicativos mobile.

### Objetivos

Esta nova API REST será crucial para ações de marketing da empresa
para aumentar nossas vendas, além de proporcionar uma melhor usabilidade aos nossos clientes.
A nova API vai ter um grande volume de requisições portanto a preocupação
com a performance e escolhas da tecnologias para entregar a performance exigida é algo de grande importância.

## Arquitetura proposta

### C4 model - Context Diagram

![Context Diagram](./docs/architecture/context-diagram.png)

### C4 model - Container Diagram

![Container Diagram](./docs/architecture/container-diagram.png)

## Planejamento e implementação

### Arquitetura e Design

A arquitetura do projeto foi baseada em Clean Arquitecture para possibitar uma divisão bem definida das camadas de presentation, domain e infra. Através dessa arquitetura foi possível desenvolver a maioria das regras de negócio da camanda de domain sem se preocupar inicialmente com as tecnologias como banco de dados, framework e libs que vão ser utilizadas no projeto, portanto após a implementação das regras de negócio foi possível fazer uma escolha mais acertiva das tecnologias.

Os testes da camada de domain também foram feitos utilizando um banco de dados em memória e com a utilização do princípio de inversão de dependência ("D" do princípio SOLID) e DI pattern (Dependency Injection) foi possível abstrair toda camada de infra(repositories) e futuramente caso o servidor de banco de dados seja alterado as regras de negócio estão intactas sem conhecimento algum da implementação concreta dos respositories.

### Language

A linguagem escolhida para desenvolver o projeto foi [Javascript/Node.js](https://nodejs.org/en/) puro para mostrar meu conhecimento sem a utilização do typescript e pelo fato dos projetos do time de Back-end Mobile serem na sua maioria em Node.js.

### Database

O banco de dados [MongoDB](https://www.mongodb.com) foi elegido para esse projeto devido os dados não necessitarem de relacionamentos com vários joins de dados. Além disso possibilitar uma escala horizontal com o uso de shards do MongoDB.

### Framework

Foi escolhido o framework [Fastify](https://www.fastify.io) devido ao foco do projeto serm em performance e após analisar [alguns benchmarks](https://www.fastify.io/benchmarks/) o Fastify foi a escolha mais acertiva, além de proporcionar vários recursos interessantes como especificação de schemas e uma integração muito bom com Open API/Swagger para geração automática da documentação da API.

### ORM

Nesse projeto não foi utilizado nenhum ORM com a finalidade de ter um overhead mínimo e não impactar na performance, além do projeto não necessitar de um ORM pelo seu escopo e complexidade baixa.
Portanto foi utilizado um [driver nativo do MongoDB para Node.js](https://www.npmjs.com/package/mongodb).

## Tech Stack & Dependencies

| Dependencies                      | Version    | Purpose                                                                                                                     |
| --------------------------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------- |
| `node`                            | `^14.17.6` | Plataforma que permite a execução de códigos JavaScript fora de um navegador web (server-side).                             |
| `fastify`                         | `^3.21.0`  | Framework para Node com foco em performance e baixo overhead                                                                |
| `fastify-autoload`                | `^3.9.0`   | Plugin para carregamento dinâmico das rotas                                                                                 |
| `fastify-swagger`                 | `^4.12.0`  | Plugin responsável pela geração da documentação OpenAPI pelas rotas e schemas                                               |
| `nodemon`                         | `^2.0.12`  | Monitorar e reiniciar o servidor em desenvolvimento caso detecte alguma mudança nos arquivos                                |
| `husky`                           | `^7.0.0`   | Configura os hooks do git permitindo rodar testes, linter e commit linter antes do commit                                   |
| `eslint`                          | `^7.32.0`  | Permite identificar erros communs e quanto ao padrão/estilo de escrita de código                                            |
| `prettier`                        | `^2.4.0`   | Formatador de código opinativo utilizado para padronizar o estilo de código                                                 |
| `lint-staged`                     | `^11.1.2`  | Permite rodar linters como eslint e prettier em arquivos staged do git                                                      |
| `@commitlint/config-conventional` | `^13.1.0`  | Possibilita a validação de sintaxe das mensagens de commit em conjunto com o husky                                          |
| `dotenv`                          | `^10.0.0`  | Ferramenta responsável por orquestrar as variáveis ambiente do projeto                                                      |
| `jest`                            | `^27.2.0`  | Responsável por criar e gerenciar todos os testes unitários do projeto                                                      |
| `axios`                           | `^0.21.4`  | Client HTTP para efetuar requisições à [API de produtos](https://gist.github.com/Bgouveia/9e043a3eba439489a35e70d1b5ea08ec) |
| `bcrypt`                          | `^5.0.1`   | Responsável por fazer fazer o hash e comparação e senhas cryptografadas                                                     |
| `http-errors`                     | `^1.8.0`   | Responsável pela criação de errors HTTP com uma sintaxe mais legível e através de exceções                                  |
| `jsonwebtoken`                    | `^8.5.1`   | Responsável pelo gerenciamento de autenticação JWT desde a criação de tokens, até a decodificação do payload de dados       |
| `uuid`                            | `^8.3.2`   | Possibilita a geração de [RFC4122](http://www.ietf.org/rfc/rfc4122.txt) UUIDs desde do padrao v1 ao v4                      |
| `mongodb`                         | `^4.1.2`   | Driver nativo do banco de dados MongoDB opensource NoSQL orientado a documentos                                             |

## Prerequirements to run the project

- Install [Docker](https://docs.docker.com/engine/install)
- Install [Docker Compose](https://docs.docker.com/compose/install)

## Running the project using Docker (Recommended)

```bash
# Running in "production" mode:
$ docker-compose up -d --build
# Running in development mode:
$ docker-compose -f docker-compose.dev.yml up --build
```

> Acesse o link do Swagger/OpenAPI: http://localhost:3000/docs

## Authentication e Authorization

A maioria dos endpoints dessa API utiliza um token JWT para realizar a autenticação, portanto verique os endpoints de `user`na documentação da API para criar um usuário e obter o token de acesso. Ao acionar os endpoints de registro e login de usuário vai ser retornado um token JWT que deve ser passado no seguinte formato "`Bearer <token>`" no header "`Authorization`" da requisição.

## Running the tests

```bash
# Connect to the container shell
$ docker exec -it node-api-wishlist sh
# Unit tests with coverage
$ npm run test
# Unit tests with coverage in watch mode
$ npm run test:dev
```

## API Docs OpenAPI

A API foi documentada utilizando o OpenAPI/Swagger que foi gerada a partir das rotas e schemas do Fastify e pode ser acessada no link abaixo:

> http://localhost:3000/docs

## API Docs Postman

Além do OpenAPI na pasta "docs/postman" do projeto também existe um arquivo de collection do [Postman](https://www.postman.com/downloads/) que pode ser utilizado para executar a API.

## Products API

Esse projeto utiliza a API de produtos fornecido pelo Luiza Labs e a documentação pode ser acessada neste link: [documentação de API de Produtos](https://gist.github.com/Bgouveia/9e043a3eba439489a35e70d1b5ea08ec)

## Future improvements for the project

- Adicionar testes E2E (End to End) utilizando a lib [Supertest](https://www.npmjs.com/package/supertest)
- Adicionar uma estratégia de cache utilizando o [Redis](https://redis.io/) como armazenamento de dados em cache
  em conjunto com o [Pattern Observer](https://pt.wikipedia.org/wiki/Observer) para que quando um dado seja inserido, alterado ou excluído a camada de cache seja acionada e não tenha uma dependência direta com a camada de repositories ou domain

## License

[MIT](LICENSE)
