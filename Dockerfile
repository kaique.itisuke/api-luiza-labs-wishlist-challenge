FROM node:14.17.6-alpine3.14

WORKDIR /usr/app/api-wishlist

ENV PORT=3000

COPY package*.json ./
RUN npm i

COPY . .

ENTRYPOINT [ "npm", "start" ]
