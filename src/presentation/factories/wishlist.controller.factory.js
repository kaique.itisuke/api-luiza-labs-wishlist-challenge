import AddProductWishlistUsecase from '../../domain/wishlist/add-product-wishlist.usecase.js'
import GetProductWishlistUsecase from '../../domain/wishlist/get-client-wishlist.usecase.js'
import RemoveProductWishlist from '../../domain/wishlist/remove-product-wishlist.usecase.js'
import ClientRepositoryMongo from '../../infra/repositories/client.repository.mongo.js'
import ProductRepositoryAxios from '../../infra/repositories/product.repository.axios.js'
import WishlistController from '../controllers/wishlist.controller.js'

export default class WishlistControllerFactory {
  static getInstance() {
    const clientRepo = new ClientRepositoryMongo()
    const productRepo = new ProductRepositoryAxios()
    const addUsecase = new AddProductWishlistUsecase(clientRepo, productRepo)
    const getUsecase = new GetProductWishlistUsecase(clientRepo)
    const removeUsecase = new RemoveProductWishlist(clientRepo, productRepo)
    const wishlistController = new WishlistController(
      addUsecase,
      getUsecase,
      removeUsecase
    )

    return wishlistController
  }
}
