import CreateClientUsecase from '../../domain/client/create-client.usecase.js'
import ClientController from '../controllers/client.controller.js'
import FindClientUsecase from '../../domain/client/find-client.usecase.js'
import UpdateClientUsecase from '../../domain/client/update-client.usecase.js'
import GetClientUsecase from '../../domain/client/get-client.usecase.js'
import RemoveClientUsecase from '../../domain/client/remove-client.usecase.js'
import ClientRepositoryMongo from '../../infra/repositories/client.repository.mongo.js'

export default class ClientControllerFactory {
  static getInstance() {
    const repository = new ClientRepositoryMongo()
    const createUsecase = new CreateClientUsecase(repository)
    const findUsecase = new FindClientUsecase(repository)
    const updateUsecase = new UpdateClientUsecase(repository)
    const getUsecase = new GetClientUsecase(repository)
    const removeUsecase = new RemoveClientUsecase(repository)
    const clientController = new ClientController(
      createUsecase,
      findUsecase,
      updateUsecase,
      getUsecase,
      removeUsecase
    )

    return clientController
  }
}
