import CreateUserUsecase from '../../domain/user/create-user.usecase.js'
import UserController from '../controllers/user.controller.js'
import LoginUserUsecase from '../../domain/user/login-user.usecase.js'
import UserRepositoryMongo from '../../infra/repositories/user.repository.mongo.js'
import PasswordBcryptEncrypter from '../../infra/encrpyters/password.bcrypt.encrypter.js'
import JwtAuthenticator from '../../infra/authentication/jwt.authenticator.js'

export default class UserControllerFactory {
  static getInstance() {
    const userRepository = new UserRepositoryMongo()
    const passwordEncrypter = new PasswordBcryptEncrypter()
    const tokenAuthenticator = new JwtAuthenticator()
    const createUsecase = new CreateUserUsecase(
      userRepository,
      passwordEncrypter,
      tokenAuthenticator
    )
    const loginUsecase = new LoginUserUsecase(
      userRepository,
      passwordEncrypter,
      tokenAuthenticator
    )
    const userController = new UserController(createUsecase, loginUsecase)

    return userController
  }
}
