import UserControllerFactory from '../factories/user.controller.factory.js'
import { createUserSchema, loginUserSchema } from '../schemas/user.schema.js'

export const autoPrefix = '/user'

export default async function clientRoutes(app, opts) {
  const controller = UserControllerFactory.getInstance()

  app.post('/', createUserSchema, controller.create)
  app.post('/login', loginUserSchema, controller.login)
}
