import WishlistControllerFactory from '../factories/wishlist.controller.factory.js'
import { applyJwtMiddleware } from '../middlewares/jwt-auth.middleware.js'
import {
  addWishlistSchema,
  getWishlistSchema,
  removeWishlistSchema,
} from '../schemas/wishlist.schema.js'

export const autoPrefix = '/wishlist'

export default async function clientRoutes(app, opts) {
  applyJwtMiddleware(app)
  const controller = WishlistControllerFactory.getInstance()

  app.post('/:clientId/:productId', addWishlistSchema, controller.add)
  app.get('/:clientId', getWishlistSchema, controller.get)
  app.delete('/:clientId/:productId', removeWishlistSchema, controller.remove)
}
