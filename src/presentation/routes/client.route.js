import ClientControllerFactory from '../factories/client.controller.factory.js'
import { applyJwtMiddleware } from '../middlewares/jwt-auth.middleware.js'
import {
  createClientSchema,
  findClientSchema,
  updateClientSchema,
  getClientSchema,
  removeClientSchema,
} from '../schemas/client.schema.js'

export const autoPrefix = '/client'

export default async function clientRoutes(app, opts) {
  applyJwtMiddleware(app)
  const controller = ClientControllerFactory.getInstance()

  app.post('/', createClientSchema, controller.create)
  app.get('/:id', findClientSchema, controller.find)
  app.put('/:id', updateClientSchema, controller.update)
  app.get('/', getClientSchema, controller.get)
  app.delete('/:id', removeClientSchema, controller.remove)
}
