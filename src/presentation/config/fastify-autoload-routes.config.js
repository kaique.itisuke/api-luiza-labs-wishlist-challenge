import { fileURLToPath } from 'url'
import { dirname, join } from 'path'
import autoLoad from 'fastify-autoload'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const fastifyAutloadRoutes = async (fastify) => {
  fastify.register(autoLoad, {
    dir: join(__dirname, '../', 'routes'),
    options: { prefix: '/api/v1' },
    recursive: false,
  })
}

export default fastifyAutloadRoutes
