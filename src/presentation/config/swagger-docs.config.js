import fastifySwagger from 'fastify-swagger'

const webserverHost = `localhost:${process.env.WEB_SERVER_PORT}`

const swaggerDocsConfig = async (fastify) => {
  fastify.register(fastifySwagger, {
    routePrefix: '/docs',
    swagger: {
      info: {
        title: 'API Luiza Labs Wishlist',
        description:
          'Documentação da nova API que irá gerenciar nossos clientes e seus produtos favoritos',
        version: '1.0.0',
      },
      host: webserverHost,
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'user', description: 'User related end-points' },
        { name: 'client', description: 'Client related end-points' },
        { name: 'wishlist', description: 'Wishlist related end-points' },
      ],
      definitions: {
        user: {
          type: 'object',
          required: ['email', 'password'],
          properties: {
            id: { type: 'string', format: 'uuid' },
            email: { type: 'string', format: 'email' },
            password: { type: 'string', format: 'password' },
          },
        },
        client: {
          type: 'object',
          required: ['name', 'email'],
          properties: {
            id: { type: 'string', format: 'uuid' },
            name: { type: 'string' },
            email: { type: 'string', format: 'email' },
          },
        },
        product: {
          type: 'object',
          required: ['title', 'brand', 'price', 'image'],
          properties: {
            id: { type: 'string', format: 'uuid' },
            title: { type: 'string' },
            brand: { type: 'string' },
            price: { type: 'number', format: 'double' },
            image: { type: 'string', format: 'uri' },
            reviewScore: { type: 'number', format: 'double' },
          },
        },
      },
      securityDefinitions: {
        apiKey: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header',
        },
      },
    },
    staticCSP: true,
    exposeRoute: true,
  })
}

export default swaggerDocsConfig
