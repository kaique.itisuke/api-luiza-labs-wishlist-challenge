import CreateUserUsecase from '../../domain/user/create-user.usecase.js'
import LoginUserUsecase from '../../domain/user/login-user.usecase.js'

export default class UserController {
  /**
   *
   * @param { CreateUserUsecase } createUserUsecase
   * @param { LoginUserUsecase } loginUserUsecase
   */
  constructor(createUserUsecase, loginUserUsecase) {
    this.createUserUsecase = createUserUsecase
    this.loginUserUsecase = loginUserUsecase
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  create = async (request, reply) => {
    const email = request.body?.email
    const password = request.body?.password
    const passwordConfirmation = request.body?.passwordConfirmation

    const response = await this.createUserUsecase.execute(
      email,
      password,
      passwordConfirmation
    )
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  login = async (request, reply) => {
    const email = request.body?.email
    const password = request.body?.password

    const response = await this.loginUserUsecase.execute(email, password)
    reply.send(response)
  }
}
