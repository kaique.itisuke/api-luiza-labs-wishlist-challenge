import CreateClientUsecase from '../../domain/client/create-client.usecase.js'
import FindClientUsecase from '../../domain/client/find-client.usecase.js'
import UpdateClientUsecase from '../../domain/client/update-client.usecase.js'
import GetClientUsecase from '../../domain/client/get-client.usecase.js'
import RemoveClientUsecase from '../../domain/client/remove-client.usecase.js'
import { HttpCodeNoContent } from '../../helpers/http/http.helper.js'

export default class ClientController {
  /**
   *
   * @param { CreateClientUsecase } createClientUsecase
   * @param { FindClientUsecase } findClientUsecase
   * @param { UpdateClientUsecase } updateClientUsecase
   * @param { GetClientUsecase } getClientUsecase
   * @param { RemoveClientUsecase } removeClientUsecase
   */
  constructor(
    createClientUsecase,
    findClientUsecase,
    updateClientUsecase,
    getClientUsecase,
    removeClientUsecase
  ) {
    this.createClientUsecase = createClientUsecase
    this.findClientUsecase = findClientUsecase
    this.updateClientUsecase = updateClientUsecase
    this.getClientUsecase = getClientUsecase
    this.removeClientUsecase = removeClientUsecase
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  create = async (request, reply) => {
    const name = request.body?.name
    const email = request.body?.email

    const response = await this.createClientUsecase.execute(name, email)
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  find = async (request, reply) => {
    const id = request.params?.id

    const response = await this.findClientUsecase.execute(id)
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  update = async (request, reply) => {
    const id = request.params?.id
    const name = request.body?.name
    const email = request.body?.email

    const response = await this.updateClientUsecase.execute(id, name, email)
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  get = async (request, reply) => {
    const response = await this.getClientUsecase.execute()
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  remove = async (request, reply) => {
    const id = request.params?.id

    await this.removeClientUsecase.execute(id)
    reply.code(HttpCodeNoContent).send()
  }
}
