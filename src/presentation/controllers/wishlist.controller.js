import AddProductWishlistUsecase from '../../domain/wishlist/add-product-wishlist.usecase.js'
import GetProductWishlistUsecase from '../../domain/wishlist/get-client-wishlist.usecase.js'
import RemoveProductWishlist from '../../domain/wishlist/remove-product-wishlist.usecase.js'
import { HttpCodeNoContent } from '../../helpers/http/http.helper.js'

export default class WishlistController {
  /**
   *
   * @param { AddProductWishlistUsecase } addProductWishlistUsecase
   * @param { GetProductWishlistUsecase } getProductWishlistUsecase
   * @param { RemoveProductWishlist } removeProductWishlist
   */
  constructor(
    addProductWishlistUsecase,
    getProductWishlistUsecase,
    removeProductWishlist
  ) {
    this.addProductWishlistUsecase = addProductWishlistUsecase
    this.getProductWishlistUsecase = getProductWishlistUsecase
    this.removeProductWishlist = removeProductWishlist
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  add = async (request, reply) => {
    const clientId = request.params?.clientId
    const productId = request.params?.productId

    const response = await this.addProductWishlistUsecase.execute(
      clientId,
      productId
    )
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  get = async (request, reply) => {
    const clientId = request.params?.clientId

    const response = await this.getProductWishlistUsecase.execute(clientId)
    reply.send(response)
  }

  /**
   *
   * @param { FastifyRequest } request
   * @param { FastifyReply } reply
   */
  remove = async (request, reply) => {
    const clientId = request.params?.clientId
    const productId = request.params?.productId

    await this.removeProductWishlist.execute(clientId, productId)
    reply.code(HttpCodeNoContent).send()
  }
}
