import JwtAuthenticator from '../../infra/authentication/jwt.authenticator.js'
import pkg from 'http-errors'
const { Unauthorized } = pkg

const jwtAuthMiddleware = async (request, reply, next) => {
  const { authorization } = request.headers

  if (!authorization) {
    throw new Unauthorized(
      'An authorization token must be provided to access this route'
    )
  }

  let token = authorization

  if (token.startsWith('Bearer ')) {
    token = authorization.substring(7, authorization.length)
  }

  try {
    const jwt = new JwtAuthenticator()
    jwt.decode(token)
  } catch (error) {
    throw new Unauthorized('Invalid JWT token')
  }
}

export const applyJwtMiddleware = async (fastify) => {
  fastify.addHook('preHandler', async (req, res) => {
    await jwtAuthMiddleware(req, res)
  })
}

export default jwtAuthMiddleware
