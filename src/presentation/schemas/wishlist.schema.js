export const addWishlistSchema = {
  schema: {
    tags: ['wishlist'],
    summary: "Add one new product to a client's wishlist",
    params: {
      type: 'object',
      properties: {
        clientId: {
          type: 'string',
          description: "Client's ID (uuid)",
        },
        productId: {
          type: 'string',
          description: "Product's ID (uuid)",
        },
      },
    },
    response: {
      200: {
        description:
          "Successful Response, the product has been added to the client's wishlist",
        type: 'object',
        properties: {
          id: { type: 'string' },
          title: { type: 'string' },
          brand: { type: 'string' },
          price: { type: 'number' },
          image: { type: 'string' },
          reviewScore: { type: 'number' },
        },
        example: {
          id: '571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f',
          title: 'Churrasqueira Elétrica Mondial 1800W',
          brand: 'mondial',
          price: 159,
          image:
            'http://challenge-api.luizalabs.com/images/571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f.jpg',
          reviewScore: 4.352941,
        },
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided Client's ID or Product's ID doesn't exist",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {b4e35a27-e162-4b90-a9d3-d3a4c144573f} does not exist',
        },
      },
      409: {
        description:
          "Conflict, when the provied Product's ID is already added to the wishlist",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 409,
          error: 'Conflict',
          message:
            'The product 1c95d400-9847-eda3-de07-0e62d80a30c6 is already added to the wishlist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: id has an invalid uuid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const getWishlistSchema = {
  schema: {
    tags: ['wishlist'],
    summary: "Get all the products from a client's wishlist",
    params: {
      type: 'object',
      properties: {
        clientId: {
          type: 'string',
          description: "Client's ID (uuid)",
        },
      },
    },
    response: {
      200: {
        description:
          "Successful Response, the client's wishlist has been found in the database",
        type: 'array',
        properties: {
          id: { type: 'string' },
          title: { type: 'string' },
          brand: { type: 'string' },
          price: { type: 'number' },
          image: { type: 'string' },
          reviewScore: { type: 'number' },
        },
        example: [
          {
            id: 'f6c094e1-f27d-677b-4187-cf6a5acd03aa',
            title: 'Taça para Vinho 1 Peça',
            brand: 'ruvolo',
            price: 39,
            image:
              'http://challenge-api.luizalabs.com/images/f6c094e1-f27d-677b-4187-cf6a5acd03aa.jpg',
          },
          {
            id: 'a0e4aa47-b17c-f266-f4d6-aba26ec085aa',
            title: 'Bonecos Família dos Cães Sylvanian Families',
            brand: 'epoch magia',
            price: 82.9,
            image:
              'http://challenge-api.luizalabs.com/images/a0e4aa47-b17c-f266-f4d6-aba26ec085aa.jpg',
          },
          {
            id: '571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f',
            title: 'Churrasqueira Elétrica Mondial 1800W',
            brand: 'mondial',
            price: 159,
            image:
              'http://challenge-api.luizalabs.com/images/571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f.jpg',
            reviewScore: 4.352941,
          },
        ],
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided client's ID doesn't exist on the database",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {b4e35a27-e162-4b90-a9d3-d3a4c144573f} does not exist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: id has an invalid uuid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const removeWishlistSchema = {
  schema: {
    tags: ['wishlist'],
    summary: "Remove one product from a client's wishlist",
    params: {
      type: 'object',
      properties: {
        clientId: {
          type: 'string',
          description: "Client's ID (uuid)",
        },
        productId: {
          type: 'string',
          description: "Product's ID (uuid)",
        },
      },
    },
    response: {
      204: {
        description:
          "Successful With No Content, the product has been removed from the client's wishlist",
        type: 'object',
        properties: {},
        example: {},
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided Client's ID or Product's ID doesn't exist",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {b4e35a27-e162-4b90-a9d3-d3a4c144573f} does not exist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: id has an invalid uuid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}
