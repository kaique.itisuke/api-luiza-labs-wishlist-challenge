export const createUserSchema = {
  schema: {
    tags: ['user'],
    summary: 'Create a new user',
    body: {
      type: 'object',
      properties: {
        email: { type: 'string' },
        password: { type: 'string' },
      },
      example: {
        email: 'jhon@user.com',
        password: '123456',
      },
    },
    response: {
      200: {
        description:
          'Successful Response, the user has been persisted in the database',
        type: 'object',
        properties: {
          id: { type: 'string' },
          email: { type: 'string' },
          token: { type: 'string' },
        },
        example: {
          id: '5dc7fe1b-6b51-40d9-bfc8-7bed38516c51',
          email: 'jhon@user.com',
          token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJkNzRiMTU5Yi1lZmM0LTQzZDEtYjBlZi1hMzM4NGJjMjliMjUiLCJpYXQiOjE2MzIxMDQxOTMsImV4cCI6MTYzMjEwNzc5M30.4v3h5vnu6lxFpylSPPreFao5TJkpcc30K743GckgVWE',
        },
      },
      409: {
        description:
          'Conflict, when the provied email already exist in the database',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 409,
          error: 'Conflict',
          message: 'The e-mail jhon@user.com is already in use',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: email is invalid',
        },
      },
    },
    security: [],
  },
}

export const loginUserSchema = {
  schema: {
    tags: ['user'],
    summary: 'Login to get a new JWT Token',
    body: {
      type: 'object',
      properties: {
        email: { type: 'string' },
        password: { type: 'string' },
      },
      example: {
        email: 'jhon@user.com',
        password: '123456',
      },
    },
    response: {
      200: {
        description:
          'Successful Response, the credentials are correct and a new JWT Token is provided',
        type: 'object',
        properties: {
          id: { type: 'string' },
          email: { type: 'string' },
          token: { type: 'string' },
        },
        example: {
          id: '5dc7fe1b-6b51-40d9-bfc8-7bed38516c51',
          email: 'jhon@user.com',
          token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJkNzRiMTU5Yi1lZmM0LTQzZDEtYjBlZi1hMzM4NGJjMjliMjUiLCJpYXQiOjE2MzIxMDM2NTMsImV4cCI6MTYzMjEwNzI1M30.6WYTrQ_Q7uqWbvNJEuPiuzvzwCDbJtZjCeLaEYNaQD0',
        },
      },
      404: {
        description:
          "Not Found, when the provided email doesn't exist on the database",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message: 'User with email: {jhon@notfound.com} does not exist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: email is invalid',
        },
      },
    },
  },
}
