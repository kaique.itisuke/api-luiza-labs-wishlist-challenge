export const createClientSchema = {
  schema: {
    tags: ['client'],
    summary: 'Create a new client',
    body: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        email: { type: 'string' },
      },
      example: {
        name: 'Jane Doe',
        email: 'jane@doe.com',
      },
    },
    response: {
      200: {
        description:
          'Successful Response, the client has been persisted in the database',
        type: 'object',
        properties: {
          id: { type: 'string' },
          name: { type: 'string' },
          email: { type: 'string' },
        },
        example: {
          id: 'a39187ce-78f5-4305-905a-f76b6460403b',
          name: 'Jane Doe',
          email: 'jane@doe.com',
        },
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      409: {
        description:
          'Conflict, when the provied email already exist in the database',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 409,
          error: 'Conflict',
          message: 'The e-mail jane@doe.com is already in use',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: email is invalid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const findClientSchema = {
  schema: {
    tags: ['client'],
    summary: 'Get one client by ID (uuid)',
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
          description: 'Client ID (uuid)',
        },
      },
    },
    response: {
      200: {
        description:
          'Successful Response, the client has been found in the database',
        type: 'object',
        properties: {
          id: { type: 'string' },
          name: { type: 'string' },
          email: { type: 'string' },
        },
        example: {
          id: 'a39187ce-78f5-4305-905a-f76b6460403b',
          name: 'Jane Updated',
          email: 'jane_updated@doe.com',
        },
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided ID doesn't exist on the database",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {dc48352a-420c-49c8-9383-a229de9f9836} does not exist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: id has an invalid uuid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const updateClientSchema = {
  schema: {
    tags: ['client'],
    summary: 'Update a new client',
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
          description: 'Client ID (uuid)',
        },
      },
    },
    body: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        email: { type: 'string' },
      },
      example: {
        name: 'Jane Updated',
        email: 'jane_updated@doe.com',
      },
    },
    response: {
      200: {
        description:
          'Successful Response, the client has been updated in the database',
        type: 'object',
        properties: {
          id: { type: 'string' },
          name: { type: 'string' },
          email: { type: 'string' },
        },
        example: {
          id: 'a39187ce-78f5-4305-905a-f76b6460403b',
          name: 'Jane Updated',
          email: 'jane_updated@doe.com',
        },
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided ID doesn't exist on the database",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {dc48352a-420c-49c8-9383-a229de9f9836} does not exist',
        },
      },
      409: {
        description:
          'Conflict, when the provied email already exist in the database',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 409,
          error: 'Conflict',
          message: 'The e-mail j@doe.com is already in use',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: email is invalid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const getClientSchema = {
  schema: {
    tags: ['client'],
    summary: 'Get a list of clients',
    response: {
      200: {
        description:
          'Successful Response, the clients were found in the database',
        type: 'array',
        properties: {
          id: { type: 'string' },
          name: { type: 'string' },
          email: { type: 'string' },
        },
        example: [
          {
            id: 'a39187ce-78f5-4305-905a-f76b6460403b',
            name: 'Jane Updated',
            email: 'jane_updated@doe.com',
          },
          {
            id: 'e41ac50d-f762-47dd-a0d2-46ae03001dfa',
            name: 'Jhon Doe',
            email: 'jhon@doe.com',
          },
        ],
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}

export const removeClientSchema = {
  schema: {
    tags: ['client'],
    summary: 'Remove one client by ID (uuid)',
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
          description: 'Client ID (uuid)',
        },
      },
    },
    response: {
      204: {
        description:
          'Successful With No Content, the client has been removed from the database',
        type: 'object',
        properties: {},
        example: {},
      },
      401: {
        description:
          'Unauthorized, when the token is invalid or is not provided in the "Authorization" header',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 401,
          error: 'Unauthorized',
          message:
            'An authorization token must be provided to access this route',
        },
      },
      404: {
        description:
          "Not Found, when the provided ID doesn't exist on the database",
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 404,
          error: 'Not Found',
          message:
            'Client with ID: {dc48352a-420c-49c8-9383-a229de9f9836} does not exist',
        },
      },
      422: {
        description:
          'Unprocessable Entity, when one or more fields are missing or has invalid values',
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
        example: {
          statusCode: 422,
          error: 'Unprocessable Entity',
          message: 'The field: id has an invalid uuid',
        },
      },
    },
    security: [
      {
        apiKey: [],
      },
    ],
  },
}
