import { Db, MongoClient } from 'mongodb'
import dotenv from 'dotenv'

dotenv.config()

/**
 *
 * @returns { Db }
 */
async function dbConnection() {
  try {
    const client = new MongoClient(process.env.MONGO_CONN_STRING)
    await client.connect()
    const db = client.db(process.env.MONGO_DB_NAME)
    console.info('Connected successfully to MongoDB server')

    return db
  } catch (err) {
    console.error('MONGODB_CONNECTION_ERROR', err)
    process.exit(1)
  }
}

export default dbConnection
