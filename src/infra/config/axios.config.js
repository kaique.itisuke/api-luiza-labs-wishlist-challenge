import axios from 'axios'

/**
 *
 * @param { string } baseUrl
 * @returns { AxiosInstance }
 */
function createAxiosInstance(baseUrl) {
  return axios.create({
    baseURL: baseUrl,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  })
}

export default createAxiosInstance
