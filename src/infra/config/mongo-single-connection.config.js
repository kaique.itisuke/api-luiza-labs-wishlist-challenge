import { Db } from 'mongodb'
import { PrivateConstructorError } from '../../helpers/errors/custom.error.js'
import dbConnection from './mongo-connection.config.js'

export default class MongoSingleConnection {
  static instance = null

  constructor() {
    throw new PrivateConstructorError()
  }

  static async init() {
    if (MongoSingleConnection.instance) {
      return
    }

    MongoSingleConnection.instance = await dbConnection()
  }

  /**
   *
   * @returns { Db }
   */
  static async getInstance() {
    await MongoSingleConnection.init()

    return MongoSingleConnection.instance
  }
}
