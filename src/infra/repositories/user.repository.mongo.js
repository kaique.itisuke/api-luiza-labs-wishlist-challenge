import UserAdapter from '../../adapters/user.adapter.js'
import UserEntity from '../../domain/user/user.entity.js'
import UserRepositoryPort from '../../domain/user/user.repository.port.js'
import { v4 as uuidv4 } from 'uuid'
import MongoSingleConnection from '../config/mongo-single-connection.config.js'

const mongoCollectionName = 'user'

export default class UserRepositoryMongo extends UserRepositoryPort {
  /**
   *
   * @returns { Collection }
   */
  async getCollection() {
    const connection = await MongoSingleConnection.getInstance()
    return connection.collection(mongoCollectionName)
  }
  /**
   *
   * @param { string } email
   * @param { string } password
   * @returns { Promise<UserEntity> }
   */
  async create(email, password) {
    const collection = await this.getCollection()
    const input = { _id: uuidv4(), email, password }
    const { insertedId } = await collection.insertOne(input)

    return UserAdapter.create(email, password, insertedId)
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<UserEntity> }
   */
  async getById(id) {
    const query = { _id: id }
    const collection = await this.getCollection()
    const result = await collection.findOne(query)

    if (!result) {
      return null
    }
    const { email, password } = result

    return UserAdapter.create(email, password, id)
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<UserEntity> }
   */
  async getByEmail(email) {
    const query = { email }
    const collection = await this.getCollection()
    const result = await collection.findOne(query)

    if (!result) {
      return null
    }
    const { _id, password } = result

    return UserAdapter.create(email, password, _id)
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    const query = { email }
    const collection = await this.getCollection()
    const counter = await collection.count(query)

    return counter
  }
}
