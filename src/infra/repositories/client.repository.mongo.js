import ClientAdapter from '../../adapters/client.adapter.js'
import ClientEntity from '../../domain/client/client.entity.js'
import ClientRepositoryPort from '../../domain/client/client.repository.port.js'
import ProductEntity from '../../domain/product/product.entity.js'
import MongoSingleConnection from '../config/mongo-single-connection.config.js'
import { v4 as uuidv4 } from 'uuid'
import ProductAdapter from '../../adapters/product.adapter.js'
import { removeEmptyFields } from '../../helpers/utils/object.util.js'

const mongoCollectionName = 'client'

export default class ClientRepositoryMongo extends ClientRepositoryPort {
  /**
   *
   * @returns { Collection }
   */
  async getCollection() {
    const connection = await MongoSingleConnection.getInstance()
    return connection.collection(mongoCollectionName)
  }

  /**
   *
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async create(name, email) {
    const collection = await this.getCollection()
    const input = { _id: uuidv4(), name, email }
    const { insertedId } = await collection.insertOne(input)

    return ClientAdapter.create(name, email, insertedId)
  }

  /**
   *
   * @param { string } id
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async update(id, name, email) {
    const query = { _id: id }
    const update = { $set: { name, email } }
    const collection = await this.getCollection()
    await collection.updateOne(query, update)

    return ClientAdapter.create(name, email, id)
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<ClientEntity> }
   */
  async getById(id) {
    const query = { _id: id }
    const collection = await this.getCollection()
    const result = await collection.findOne(query)

    if (!result) {
      return null
    }
    const { name, email } = result

    return ClientAdapter.create(name, email, id)
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    const query = { email }
    const collection = await this.getCollection()
    const counter = await collection.count(query)

    return counter
  }

  /**
   *
   * @returns { Promise<ClientEntity[]> }
   */
  async get() {
    const collection = await this.getCollection()
    const results = await collection.find({}).toArray()

    return results.map((result) => {
      const { _id, name, email } = result
      return ClientAdapter.create(name, email, _id)
    })
  }

  /**
   *
   * @param { string } id
   */
  async remove(id) {
    const query = { _id: id }
    const collection = await this.getCollection()
    await collection.deleteOne(query)
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @returns { Promise<number> }
   */
  async countProductsByClient(clientId, productId) {
    const query = {
      _id: clientId,
      'products.id': { $in: [productId] },
    }
    const collection = await this.getCollection()
    const counter = await collection.find(query).count()

    return counter
  }

  /**
   *
   * @param { string } clientId
   * @param { ProductEntity } productId
   * @returns { Promise<ClientEntity> }
   */
  async addFavoriteProduct(clientId, product) {
    product = removeEmptyFields(product)
    const query = { _id: clientId }
    const update = { $push: { products: product } }
    const collection = await this.getCollection()
    await collection.updateOne(query, update)
  }

  /**
   *
   * @param { string } clientId
   * @returns { Promise<ClientEntity[]> }
   */
  async getFavoriteProducts(clientId) {
    const query = { _id: clientId }
    const filters = { products: 1, _id: 0 }
    const collection = await this.getCollection()
    const result = await collection.findOne(query, { projection: filters })
    const products = result?.products || []

    return products.map((product) => {
      const { id, title, brand, price, image, reviewScore } = product
      return removeEmptyFields(
        ProductAdapter.create(id, title, brand, price, image, reviewScore)
      )
    })
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   */
  async removeFavoriteProduct(clientId, productId) {
    const query = { _id: clientId }
    const update = { $pull: { products: { id: productId } } }
    const collection = await this.getCollection()
    collection.update(
      query,
      update,
      false, // Upsert
      true // Multi
    )
  }
}
