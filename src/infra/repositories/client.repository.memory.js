import ClientAdapter from '../../adapters/client.adapter.js'
import ClientEntity from '../../domain/client/client.entity.js'
import ClientRepositoryPort from '../../domain/client/client.repository.port.js'
import { v4 as uuidv4 } from 'uuid'
import ProductEntity from '../../domain/product/product.entity.js'

export default class ClientRepositoryMemory extends ClientRepositoryPort {
  /**
   *
   * @param { ClientEntity[] } clients
   */
  constructor(clients = []) {
    super()
    this.clients = clients
  }

  /**
   *
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async create(name, email) {
    const newClient = ClientAdapter.create(name, email, uuidv4())
    this.clients.push(newClient)
    return newClient
  }

  /**
   *
   * @param { string } id
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async update(id, name, email) {
    const indexToUpdate = this.clients.findIndex((client) => client.id === id)
    const clientToUpdate = this.clients[indexToUpdate]
    clientToUpdate.name = name
    clientToUpdate.email = email

    return ClientAdapter.create(name, email, id)
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<ClientEntity> }
   */
  async getById(id) {
    const client = this.clients.find((client) => client.id === id)
    return client
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    return this.clients.filter((client) => client.email === email).length
  }

  /**
   *
   * @returns { Promise<ClientEntity[]> }
   */
  async get() {
    return this.clients
  }

  /**
   *
   * @param { string } id
   */
  async remove(id) {
    const indexToRemove = this.clients.findIndex((client) => client.id === id)
    this.clients.splice(indexToRemove, 1)
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @returns { Promise<number> }
   */
  async countProductsByClient(clientId, productId) {
    const { products } = await this.getById(clientId)

    if (!products) {
      return 0
    }

    return products.filter((product) => {
      return product.id === productId
    }).length
  }

  /**
   *
   * @param { string } clientId
   * @param { ProductEntity } productId
   * @returns { Promise<ClientEntity> }
   */
  async addFavoriteProduct(clientId, product) {
    const foundClient = this.clients.find((client) => client.id === clientId)

    if (!foundClient.products) {
      foundClient.products = []
    }

    foundClient.products.push(product)
  }

  /**
   *
   * @param { string } clientId
   * @returns { Promise<ClientEntity[]> }
   */
  async getFavoriteProducts(clientId) {
    const { products } = this.clients.find((client) => client.id === clientId)

    if (!products) {
      return []
    }

    return products
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   */
  async removeFavoriteProduct(clientId, productId) {
    const foundClient = this.clients.find((client) => client.id === clientId)
    const productIndexToRemove = foundClient.products.findIndex(
      (product) => product.id === productId
    )
    foundClient.products.splice(productIndexToRemove, 1)
  }
}
