import ProductAdapter from '../../adapters/product.adapter.js'
import ProductEntity from '../../domain/product/product.entity.js'
import createAxiosInstance from '../config/axios.config.js'
import dotenv from 'dotenv'

dotenv.config()

export default class ProductRepositoryAxios {
  /**
   * @returns { AxiosInstance }
   */
  getAxios() {
    const productUrl = `${process.env.PRODUCT_API_BASE_URL}/${process.env.PRODUCT_API_ENDPOINT}`
    return createAxiosInstance(productUrl)
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<ProductEntity> }
   */
  async getById(id) {
    try {
      const { data } = await this.getAxios().get(`/${id}/`)

      return ProductAdapter.create(
        data.id,
        data.title,
        data.brand,
        data.price,
        data.image,
        data?.reviewScore
      )
    } catch (error) {
      const isProductFound = error?.response?.status !== 404
      if (!isProductFound) {
        return null
      }

      throw error
    }
  }
}
