import UserAdapter from '../../adapters/user.adapter.js'
import UserEntity from '../../domain/user/user.entity.js'
import UserRepositoryPort from '../../domain/user/user.repository.port.js'
import { v4 as uuidv4 } from 'uuid'

export default class UserRepositoryMemory extends UserRepositoryPort {
  constructor() {
    super()
    this.users = []
  }

  /**
   *
   * @param { string } email
   * @param { string } password
   * @returns { Promise<UserEntity> }
   */
  async create(email, password) {
    const id = uuidv4()
    this.users.push({ email, password, id })

    return UserAdapter.create(email, password, id)
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<UserEntity> }
   */
  async getById(id) {
    const { email, password } = this.users.find((user) => user.id === id)
    return UserAdapter.create(email, password, id)
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<UserEntity> }
   */
  async getByEmail(email) {
    return this.users.find((user) => user.email === email)
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    return this.users.filter((user) => user.email === email).length
  }
}
