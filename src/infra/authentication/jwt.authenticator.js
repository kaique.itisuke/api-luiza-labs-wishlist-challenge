import TokenAuthenticatorPort from '../../domain/base/token.authenticator.port.js'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()

export default class JwtAuthenticator extends TokenAuthenticatorPort {
  /**
   *
   * @param { object } payload
   * @returns { string } token
   */
  generate(payload) {
    return jwt.sign(payload, process.env.AUTH_SECRET, {
      expiresIn: `${process.env.AUTH_EXPIRATION_SECONDS}s`,
    })
  }

  /**
   *
   * @param { string } token
   * @returns { object } payload
   */
  decode(token) {
    return jwt.verify(token, process.env.AUTH_SECRET)
  }
}
