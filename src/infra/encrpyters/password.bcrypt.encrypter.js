import PasswordEncrypterPort from '../../domain/base/password.encrypter.port.js'
import bcrypt from 'bcrypt'

export default class PasswordBcryptEncrypter extends PasswordEncrypterPort {
  /**
   *
   * @param { string } cleanPassword
   * @returns { Promise<string> } hashed password
   */
  async hash(cleanPassword) {
    const saltRounds = 10
    return bcrypt.hash(cleanPassword, saltRounds)
  }

  /**
   *
   * @param { string } cleanPassword
   * @param { string } hashedPassword
   * @returns { Promise<boolean> }
   */
  async isPasswordMatched(cleanPassword, hashedPassword) {
    return bcrypt.compare(cleanPassword, hashedPassword)
  }
}
