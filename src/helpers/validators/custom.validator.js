import {
  EqualFieldsError,
  InvalidFieldError,
  InvalidUuidError,
  NumberFieldError,
  RequiredFieldError,
} from '../errors/custom.error.js'

/**
 *
 * @param { any } value
 * @param { string } field
 * @throws { RequiredFieldError }
 * @returns { boolean }
 */
export const validateReqField = (value, field) => {
  if (!value) {
    throw new RequiredFieldError(field)
  }

  return true
}
/**
 *
 * @param { any } value
 * @param { string } field
 * @throws { EqualFieldsError }
 * @returns { boolean }
 */
export const validateEqualFields = (value1, value2, field1, field2) => {
  if (value1 !== value2) {
    throw new EqualFieldsError(field1, field2)
  }

  return true
}

/**
 *
 * @param { any } value
 * @param { string } field
 * @throws { NumberFieldError }
 * @returns { boolean }
 */
export const validateNumberField = (value, field) => {
  if (value == null) {
    return true
  }

  if (isNaN(value)) {
    throw new NumberFieldError(field)
  }

  return true
}

/**
 *
 * @param { string } value
 * @param { string } field
 * @throws { InvalidFieldError }
 * @returns { boolean }
 */
export const validateEmailField = (value, field = 'email') => {
  const isEmailValid = /\S+@\S+\.\S+/.test(value)

  if (!isEmailValid) {
    throw new InvalidFieldError(field)
  }

  return true
}

/**
 *
 * @param { string } value
 * @param { string } field
 * @throws { InvalidUuidError }
 * @returns { boolean }
 */
export const validateUuidField = (value, field = 'id') => {
  const uuidRegex =
    /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi
  const isUuidValid = uuidRegex.test(value)
  if (!isUuidValid) {
    throw new InvalidUuidError(field)
  }

  return true
}
