import pkg from 'http-errors'
const { InternalServerError, UnprocessableEntity } = pkg

export class MethodNotImplementedError extends InternalServerError {
  /**
   *
   * @param { string } method
   */
  constructor(method) {
    super(`Method ${method} is not implemented`)
    this.name = 'MethodNotImplementedError'
  }
}

export class PrivateConstructorError extends InternalServerError {
  /**
   *
   * @param { string } method
   */
  constructor(method = 'getInstance') {
    super(
      `This class can't be instantiated, use the method: ${method}() instead`
    )
    this.name = 'PrivateConstructorError'
  }
}

export class RequiredFieldError extends UnprocessableEntity {
  /**
   *
   * @param { string } field
   */
  constructor(field) {
    super(`The field: ${field} is required`)
    this.name = 'RequiredFieldError'
  }
}

export class EqualFieldsError extends UnprocessableEntity {
  /**
   *
   * @param { string } field
   */
  constructor(field1, field2) {
    super(`The field: ${field1} must be equal to field: ${field2}`)
    this.name = 'EqualFieldsError'
  }
}

export class NumberFieldError extends UnprocessableEntity {
  /**
   *
   * @param { string } field
   */
  constructor(field) {
    super(`The field: ${field} must be a number`)
    this.name = 'NumberFieldError'
  }
}

export class InvalidFieldError extends UnprocessableEntity {
  /**
   *
   * @param { string } field
   */
  constructor(field) {
    super(`The field: ${field} is invalid`)
    this.name = 'InvalidFieldError'
  }
}

export class InvalidUuidError extends UnprocessableEntity {
  /**
   *
   * @param { string } field
   */
  constructor(field) {
    super(`The field: ${field} has an invalid uuid`)
    this.name = 'InvalidUuidError'
  }
}
