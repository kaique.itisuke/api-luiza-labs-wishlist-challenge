import {
  validateEmailField,
  validateReqField,
  validateUuidField,
} from '../../helpers/validators/custom.validator.js'
import { ClientEmailAlreadyInUse, ClientNotFoundById } from './client.error.js'

export default class ClientValidator {
  /**
   *
   * @param { string } name
   * @param { string } email
   * @returns { boolean }
   */
  static validateInput(name, email) {
    validateReqField(name, 'name')
    validateReqField(email, 'email')
    validateEmailField(email)

    return true
  }

  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { string } id
   * @throws { RequiredFieldError, InvalidUuidError, ClientNotFoundById }
   * @returns { Promise<ClientEntity> }
   */
  static async validateClientExists(clientRepository, id) {
    validateReqField(id, 'id')
    validateUuidField(id)
    const client = await clientRepository.getById(id)

    if (!client) {
      throw new ClientNotFoundById(id)
    }

    return client
  }

  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { string } email
   * @throws { ClientEmailAlreadyInUse }
   * @returns { Promise<boolean> }
   */
  static async validateEmailUnique(clientRepository, email) {
    const countClientsWithSameEmail = await clientRepository.countByEmail(email)

    if (countClientsWithSameEmail > 0) {
      throw new ClientEmailAlreadyInUse(email)
    }

    return true
  }
}
