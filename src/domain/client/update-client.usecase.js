import ClientValidator from './client.validator.js'

export default class UpdateClientUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @param { string } id
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async execute(id, name, email) {
    ClientValidator.validateInput(name, email)
    const clientFound = await ClientValidator.validateClientExists(
      this.clientRepository,
      id
    )

    const isEmailChanged = clientFound.email !== email

    if (isEmailChanged) {
      await ClientValidator.validateEmailUnique(this.clientRepository, email)
    }

    return this.clientRepository.update(id, name, email)
  }
}
