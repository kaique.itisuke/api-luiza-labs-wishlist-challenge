export default class GetClientUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @returns { Promise<ClientEntity[]> }
   */
  async execute() {
    return await this.clientRepository.get()
  }
}
