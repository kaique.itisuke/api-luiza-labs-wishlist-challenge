import { MethodNotImplementedError } from '../../helpers/errors/custom.error.js'
import ProductEntity from '../product/product.entity.js'
import ClientEntity from './client.entity.js'

/**
 * The purpose of this class is to mimic a interface in order to apply
 * dependency inversion pattern from SOLI"D"
 *
 * @abstract
 */
export default class ClientRepositoryPort {
  /**
   *
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async create(name, email) {
    throw new MethodNotImplementedError('create')
  }

  /**
   *
   * @param { string } id
   * @param { string } name
   * @param { string } email
   * @returns { Promise<ClientEntity> }
   */
  async update(id, name, email) {
    throw new MethodNotImplementedError('update')
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<ClientEntity> }
   */
  async getById(id) {
    throw new MethodNotImplementedError('getById')
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    throw new MethodNotImplementedError('countByEmail')
  }

  /**
   *
   * @returns { Promise<ClientEntity[]> }
   */
  async get() {
    throw new MethodNotImplementedError('get')
  }

  /**
   *
   * @param { string } id
   */
  async remove(id) {
    throw new MethodNotImplementedError('remove')
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @returns { Promise<number> }
   */
  async countProductsByClient(clientId, productId) {
    throw new MethodNotImplementedError('countProductsByClient')
  }

  /**
   *
   * @param { string } clientId
   * @param { ProductEntity } productId
   * @returns { Promise<ClientEntity> }
   */
  async addFavoriteProduct(clientId, product) {
    throw new MethodNotImplementedError('addFavoriteProduct')
  }

  /**
   *
   * @param { string } clientId
   * @returns { Promise<ClientEntity[]> }
   */
  async getFavoriteProducts(clientId) {
    throw new MethodNotImplementedError('addFavoriteProduct')
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   */
  async removeFavoriteProduct(clientId, productId) {
    throw new MethodNotImplementedError('removeFavoriteProduct')
  }
}
