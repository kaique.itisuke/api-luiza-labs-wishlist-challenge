import ClientValidator from './client.validator.js'

export default class ClientEntity {
  /**
   *
   * @param { string } name
   * @param { string } email
   * @param { string } id
   */
  constructor(name, email, id = null) {
    ClientValidator.validateInput(name, email)

    this.name = name
    this.email = email
    this.id = id
  }
}
