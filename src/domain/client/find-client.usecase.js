import ClientValidator from './client.validator.js'

export default class FindClientUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @param { string } id
   * @throws { InvalidUuidError, ClientNotFoundById }
   * @returns { Promise<ClientEntity> }
   */
  async execute(id) {
    return await ClientValidator.validateClientExists(this.clientRepository, id)
  }
}
