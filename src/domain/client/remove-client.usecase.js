import ClientValidator from './client.validator.js'

export default class RemoveClientUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @param { string } id
   * @throws { InvalidUuidError, ClientNotFoundById }
   * @returns { Promise<ClientEntity> }
   */
  async execute(id) {
    const clientFound = await ClientValidator.validateClientExists(
      this.clientRepository,
      id
    )

    await this.clientRepository.remove(clientFound.id)
  }
}
