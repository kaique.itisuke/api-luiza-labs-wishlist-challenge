import { ClientEmailAlreadyInUse } from './client.error.js'
import ClientRepositoryPort from './client.repository.port.js'
import ClientValidator from './client.validator.js'

export default class CreateClientUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @param { string } name
   * @param { string } email
   * @throws { ClientEmailAlreadyInUse }
   * @returns { Promise<ClientEntity> }
   */
  async execute(name, email) {
    ClientValidator.validateInput(name, email)
    await ClientValidator.validateEmailUnique(this.clientRepository, email)

    return this.clientRepository.create(name, email)
  }
}
