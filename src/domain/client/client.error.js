import pkg from 'http-errors'
const { Conflict, NotFound } = pkg

export class ClientEmailAlreadyInUse extends Conflict {
  /**
   *
   * @param { string } email
   */
  constructor(email) {
    super(`The e-mail ${email} is already in use`)
    this.name = 'ClientEmailAlreadyInUse'
  }
}

export class ClientNotFoundById extends NotFound {
  /**
   *
   * @param { string } id
   */
  constructor(id) {
    super(`Client with ID: {${id}} does not exist`)
    this.name = 'ClientNotFoundById'
  }
}
