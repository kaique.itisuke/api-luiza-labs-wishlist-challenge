import pkg from 'http-errors'
const { NotFound, Conflict } = pkg

export class WishlistProductAlreadyAdded extends Conflict {
  /**
   *
   * @param { string } productId
   */
  constructor(productId) {
    super(`The product ${productId} is already added to the wishlist`)
    this.name = 'WishlistProductAlreadyAdded'
  }
}

export class WishlistProductNotExist extends NotFound {
  /**
   *
   * @param { string } productId
   */
  constructor(productId) {
    super(`The product with ID: {${productId}} does not exists in the wishlist`)
    this.name = 'WishlistProductAlreadyAdded'
  }
}
