import ProductRepositoryPort from '../product/product.repository.port.js'
import ClientRepositoryPort from '../client/client.repository.port.js'
import ClientValidator from '../client/client.validator.js'
import WishlistValidator from './wishlist.validator.js'

export default class RemoveProductWishlistUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { ProductRepositoryPort } productRepository
   */
  constructor(clientRepository, productRepository) {
    this.clientRepository = clientRepository
    this.productRepository = productRepository
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @throws { RequiredFieldError, InvalidUuidError, ClientNotFoundById, WishlistProductNotExist }
   */
  async execute(clientId, productId) {
    await ClientValidator.validateClientExists(this.clientRepository, clientId)
    await WishlistValidator.validateProductExistsWishlist(
      this.clientRepository,
      clientId,
      productId
    )

    this.clientRepository.removeFavoriteProduct(clientId, productId)
  }
}
