import ProductRepositoryPort from '../product/product.repository.port.js'
import ClientRepositoryPort from '../client/client.repository.port.js'
import ProductValidator from '../product/product.validator.js'
import ClientValidator from '../client/client.validator.js'
import WishlistValidator from './wishlist.validator.js'
import ClientEntity from '../client/client.entity.js'
import { removeEmptyFields } from '../../helpers/utils/object.util.js'

export default class AddProductWishlistUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { ProductRepositoryPort } productRepository
   */
  constructor(clientRepository, productRepository) {
    this.clientRepository = clientRepository
    this.productRepository = productRepository
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @param { Promise<ClientEntity> }
   */
  async execute(clientId, productId) {
    await ClientValidator.validateClientExists(this.clientRepository, clientId)
    await WishlistValidator.validateProductUnique(
      this.clientRepository,
      clientId,
      productId
    )
    const product = await ProductValidator.validateProductExists(
      this.productRepository,
      productId
    )

    await this.clientRepository.addFavoriteProduct(clientId, product)

    return removeEmptyFields(product)
  }
}
