import {
  WishlistProductAlreadyAdded,
  WishlistProductNotExist,
} from './wishlist.error.js'
import ClientRepositoryPort from '../client/client.repository.port.js'

export default class WishlistValidator {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { string } clientId
   * @param { string } productId
   * @throws { WishlistProductAlreadyAdded }
   * @returns { Promise<boolean> }
   */
  static async validateProductUnique(clientRepository, clientId, productId) {
    const countProductsByClient = await clientRepository.countProductsByClient(
      clientId,
      productId
    )

    if (countProductsByClient > 0) {
      throw new WishlistProductAlreadyAdded(productId)
    }

    return true
  }

  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   * @param { string } clientId
   * @param { string } productId
   * @throws { WishlistProductAlreadyAdded }
   * @returns { Promise<boolean> }
   */
  static async validateProductExistsWishlist(
    clientRepository,
    clientId,
    productId
  ) {
    const countProductsByClient = await clientRepository.countProductsByClient(
      clientId,
      productId
    )

    if (countProductsByClient == 0) {
      throw new WishlistProductNotExist(productId)
    }

    return true
  }
}
