import ClientRepositoryPort from '../client/client.repository.port.js'
import ClientValidator from '../client/client.validator.js'

export default class GetClientWishlistUsecase {
  /**
   *
   * @param { ClientRepositoryPort } clientRepository
   */
  constructor(clientRepository) {
    this.clientRepository = clientRepository
  }

  /**
   *
   * @param { string } clientId
   * @param { string } productId
   * @param { Promise<ClientEntity> }
   */
  async execute(clientId) {
    await ClientValidator.validateClientExists(this.clientRepository, clientId)

    return await this.clientRepository.getFavoriteProducts(clientId)
  }
}
