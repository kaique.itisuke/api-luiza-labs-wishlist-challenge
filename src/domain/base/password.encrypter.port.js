import { MethodNotImplementedError } from '../../helpers/errors/custom.error.js'

/**
 * The purpose of this class is to mimic a interface in order to apply
 * dependency inversion pattern from SOLI"D"
 *
 * @abstract
 */
export default class PasswordEncrypterPort {
  /**
   *
   * @param { string } cleanPassword
   * @throws { MethodNotImplementedError }
   * @returns { Promise<string> } hashed password
   */
  async hash(cleanPassword) {
    throw new MethodNotImplementedError('hash')
  }

  /**
   *
   * @param { string } cleanPassword
   * @param { string } hashedPassword
   * @throws { MethodNotImplementedError }
   * @returns { Promise<boolean> }
   */
  async isPasswordMatched(cleanPassword, hashedPassword) {
    throw new MethodNotImplementedError('isPasswordMatched')
  }
}
