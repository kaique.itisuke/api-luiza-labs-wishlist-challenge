import { MethodNotImplementedError } from '../../helpers/errors/custom.error.js'

/**
 * The purpose of this class is to mimic a interface in order to apply
 * dependency inversion pattern from SOLI"D"
 *
 * @abstract
 */
export default class TokenAuthenticatorPort {
  /**
   *
   * @param { object } payload
   * @throws { MethodNotImplementedError }
   * @returns { string } token
   */
  generate(payload) {
    throw new MethodNotImplementedError('generate')
  }

  /**
   *
   * @param { string } token
   * @throws { MethodNotImplementedError }
   * @returns { object } payload
   */
  decode(token) {
    throw new MethodNotImplementedError('decode')
  }
}
