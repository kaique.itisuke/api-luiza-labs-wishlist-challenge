import ProductEntity from './product.entity.js'
import ProductValidator from './product.validator.js'

export default class FindProductUsecase {
  /**
   *
   * @param { ProductRepositoryPort } productRepository
   */
  constructor(productRepository) {
    this.productRepository = productRepository
  }

  /**
   *
   * @param { string } id
   * @throws { InvalidUuidError, ProductNotFoundById }
   * @returns { Promise<ProductEntity> }
   */
  async execute(id) {
    return await ProductValidator.validateProductExists(
      this.productRepository,
      id
    )
  }
}
