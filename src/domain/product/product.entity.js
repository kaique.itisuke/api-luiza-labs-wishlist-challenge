import ProductValidator from './product.validator.js'

export default class ProductEntity {
  /**
   *
   * @param { string } id
   * @param { string } title
   * @param { string } brand
   * @param { number } price
   * @param { string } image
   * @param { number } reviewScore
   */
  constructor(id, title, brand, price, image, reviewScore = null) {
    ProductValidator.validateInput(id, title, brand, price, image)

    this.id = id
    this.title = title
    this.brand = brand
    this.price = price
    this.image = image
    this.reviewScore = reviewScore
  }
}
