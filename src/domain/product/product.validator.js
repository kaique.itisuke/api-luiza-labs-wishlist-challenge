import {
  validateNumberField,
  validateReqField,
  validateUuidField,
} from '../../helpers/validators/custom.validator.js'
import { ProductNotFoundById } from './product.error.js'

export default class ProductValidator {
  /**
   *
   * @param { string } id
   * @param { string } title
   * @param { string } brand
   * @param { number } price
   * @param { string } image
   * @param { number } reviewScore
   * @returns { boolean }
   */
  static validateInput(id, title, brand, price, image, reviewScore) {
    validateReqField(id, 'id')
    validateReqField(title, 'title')
    validateReqField(brand, 'brand')
    validateReqField(price, 'price')
    validateReqField(image, 'image')
    validateNumberField(reviewScore, 'reviewScore')

    return true
  }
  /**
   *
   * @param { ProductRepositoryPort } productRepository
   * @param { string } id
   * @throws { RequiredFieldError, InvalidUuidError, ProductNotFoundById }
   * @returns { Promise<ProductEntity> }
   */
  static async validateProductExists(productRepository, id) {
    validateReqField(id, 'id')
    validateUuidField(id)
    const product = await productRepository.getById(id)

    if (!product) {
      throw new ProductNotFoundById(id)
    }

    return product
  }
}
