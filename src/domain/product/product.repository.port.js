import { MethodNotImplementedError } from '../../helpers/errors/custom.error.js'
import ProductEntity from './product.entity.js'

/**
 * The purpose of this class is to mimic a interface in order to apply
 * dependency inversion pattern from SOLI"D"
 *
 * @abstract
 */
export default class ProductRepositoryPort {
  /**
   *
   * @param { string } id
   * @returns { Promise<ProductEntity> }
   */
  async getById(id) {
    throw new MethodNotImplementedError('getById')
  }
}
