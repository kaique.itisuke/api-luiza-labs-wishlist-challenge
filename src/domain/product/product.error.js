import pkg from 'http-errors'
const { NotFound } = pkg

export class ProductNotFoundById extends NotFound {
  /**
   *
   * @param { string } id
   */
  constructor(id) {
    super(`Product with ID: {${id}} does not exist`)
    this.name = 'ProductNotFoundById'
  }
}
