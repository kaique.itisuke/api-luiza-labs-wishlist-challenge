import {
  validateEmailField,
  validateEqualFields,
  validateReqField,
} from '../../helpers/validators/custom.validator.js'
import { UserEmailAlreadyInUse, UserNotFoundByEmail } from './user.error.js'
import UserRepositoryPort from './user.repository.port.js'

export default class UserValidator {
  /**
   *
   * @param { string } email
   * @param { string } password
   * @param { string } passwordConfirmation
   * @throws { RequiredFieldError, InvalidFieldError, EqualFieldsError }
   * @returns { boolean }
   */
  static validateInput(email, password, passwordConfirmation = undefined) {
    validateReqField(email, 'email')
    validateEmailField(email)
    validateReqField(password, 'password')

    if (passwordConfirmation !== undefined) {
      validateEqualFields(
        passwordConfirmation,
        password,
        'passwordConfirmation',
        'password'
      )
    }

    return true
  }

  /**
   *
   * @param { UserRepositoryPort } userRepository
   * @param { string } email
   * @throws { RequiredFieldError, InvalidUuidError, UserNotFoundByEmail }
   * @returns { Promise<UserEntity> }
   */
  static async validateUserExists(userRepository, email) {
    const user = await userRepository.getByEmail(email)

    if (!user) {
      throw new UserNotFoundByEmail(email)
    }

    return user
  }

  /**
   *
   * @param { UserRepositoryPort } userRepository
   * @param { string } email
   * @throws { UserEmailAlreadyInUse }
   * @returns { Promise<boolean> }
   */
  static async validateEmailUnique(userRepository, email) {
    const countUsersWithSameEmail = await userRepository.countByEmail(email)

    if (countUsersWithSameEmail > 0) {
      throw new UserEmailAlreadyInUse(email)
    }

    return true
  }
}
