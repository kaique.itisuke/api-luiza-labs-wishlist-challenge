import pkg from 'http-errors'
const { Conflict, NotFound, UnprocessableEntity } = pkg

export class UserEmailAlreadyInUse extends Conflict {
  /**
   *
   * @param { string } email
   */
  constructor(email) {
    super(`The e-mail ${email} is already in use`)
    this.name = 'UserEmailAlreadyInUse'
  }
}

export class UserNotFoundById extends NotFound {
  /**
   *
   * @param { string } id
   */

  constructor(id) {
    super(`User with ID: {${id}} does not exist`)
    this.name = 'UserNotFoundById'
  }
}

export class UserNotFoundByEmail extends NotFound {
  /**
   *
   * @param { string } email
   */
  constructor(email) {
    super(`User with email: {${email}} does not exist`)
    this.name = 'UserNotFoundByEmail'
  }
}

export class UserInvalidPassword extends UnprocessableEntity {
  constructor() {
    super('Invalid password')
    this.name = 'UserInvalidPassword'
  }
}
