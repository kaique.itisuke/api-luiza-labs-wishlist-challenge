import PasswordEncrypterPort from '../base/password.encrypter.port.js'
import TokenAuthenticatorPort from '../base/token.authenticator.port.js'
import { UserInvalidPassword } from './user.error.js'
import UserRepositoryPort from './user.repository.port.js'
import UserValidator from './user.validator.js'

export default class LoginUserUsecase {
  /**
   *
   * @param { UserRepositoryPort } userRepository
   * @param { PasswordEncrypterPort } passwordEncrypter
   * @param { TokenAuthenticatorPort } tokenAuthenticator
   */
  constructor(userRepository, passwordEncrypter, tokenAuthenticator) {
    this.userRepository = userRepository
    this.passwordEncrypter = passwordEncrypter
    this.tokenAuthenticator = tokenAuthenticator
  }

  /**
   *
   * @param { string } email
   * @param { string } password
   * @throws {
   *           RequiredFieldError, InvalidFieldError,
   *           EqualFieldsError, InvalidUuidError
   *           UserInvalidPassword, UserNotFoundByEmail
   *         }
   * @returns { Promise<UserEntity> }
   */
  async execute(email, password) {
    UserValidator.validateInput(email, password)
    const user = await UserValidator.validateUserExists(
      this.userRepository,
      email
    )
    const isPasswordMatched = await this.passwordEncrypter.isPasswordMatched(
      password,
      user.password
    )

    if (!isPasswordMatched) {
      throw new UserInvalidPassword()
    }

    const { id } = user
    const userPayload = { userId: id }
    const token = this.tokenAuthenticator.generate(userPayload)

    return { id, email, token }
  }
}
