export default class UserEntity {
  /**
   *
   * @param { string } email
   * @param { string } password
   * @param { string } id
   */
  constructor(email, password, id = null, token = null) {
    this.email = email
    this.password = password
    this.id = id
    this.token = token
  }
}
