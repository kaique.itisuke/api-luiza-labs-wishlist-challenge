import UserRepositoryPort from './user.repository.port.js'
import TokenAuthenticatorPort from '../base/token.authenticator.port.js'
import UserValidator from './user.validator.js'
import PasswordEncrypterPort from '../base/password.encrypter.port.js'

export default class CreateUserUsecase {
  /**
   *
   * @param { UserRepositoryPort } userRepository
   * @param { PasswordEncrypterPort } passwordEncrypter
   * @param { TokenAuthenticatorPort } tokenAuthenticator
   */
  constructor(userRepository, passwordEncrypter, tokenAuthenticator) {
    this.userRepository = userRepository
    this.passwordEncrypter = passwordEncrypter
    this.tokenAuthenticator = tokenAuthenticator
  }

  /**
   *
   * @param { string } email
   * @param { string } password
   * @param { string } passwordConfirmation
   * @throws {
   *           RequiredFieldError, InvalidFieldError,
   *           EqualFieldsError, UserEmailAlreadyInUse
   *         }
   * @returns { Promise<UserEntity> }
   */
  async execute(email, password, passwordConfirmation) {
    UserValidator.validateInput(email, password, passwordConfirmation)
    await UserValidator.validateEmailUnique(this.userRepository, email)
    const hashedPassword = await this.passwordEncrypter.hash(password)

    const { id } = await this.userRepository.create(email, hashedPassword)
    const userPayload = { userId: id }
    const token = this.tokenAuthenticator.generate(userPayload)

    return { id, email, token }
  }
}
