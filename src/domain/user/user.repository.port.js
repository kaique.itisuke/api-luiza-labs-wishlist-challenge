import { MethodNotImplementedError } from '../../helpers/errors/custom.error.js'
import UserEntity from './user.entity.js'

/**
 * The purpose of this class is to mimic a interface in order to apply
 * dependency inversion pattern from SOLI"D"
 *
 * @abstract
 */
export default class UserRepositoryPort {
  /**
   *
   * @param { string } email
   * @param { string } password
   * @returns { Promise<UserEntity> }
   */
  async create(email, password) {
    throw new MethodNotImplementedError('create')
  }

  /**
   *
   * @param { string } id
   * @returns { Promise<UserEntity> }
   */
  async getById(id) {
    throw new MethodNotImplementedError('getById')
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<UserEntity> }
   */
  async getByEmail(email) {
    throw new MethodNotImplementedError('getByEmail')
  }

  /**
   *
   * @param { string } email
   * @returns { Promise<number> }
   */
  async countByEmail(email) {
    throw new MethodNotImplementedError('countByEmail')
  }
}
