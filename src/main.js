import dotenv from 'dotenv'
import Fastify from 'fastify'
import MongoSingleConnection from './infra/config/mongo-single-connection.config.js'
import registerSwaggerDocs from './presentation/config/swagger-docs.config.js'
import registerAutoloadRoutes from './presentation/config/fastify-autoload-routes.config.js'

dotenv.config()

const fastify = Fastify({})

registerSwaggerDocs(fastify)
registerAutoloadRoutes(fastify)

const start = async () => {
  try {
    await MongoSingleConnection.init()
    await fastify.listen(
      process.env.WEB_SERVER_PORT,
      process.env.WEB_SERVER_HOST
    )

    console.info(`Running at http://localhost:${process.env.WEB_SERVER_PORT}`)
  } catch (err) {
    fastify.log.error(err)
    throw new Error(err)
  }
}
start()
