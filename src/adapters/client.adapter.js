import ClientEntity from '../domain/client/client.entity.js'

export default class ClientAdapter {
  /**
   *
   * @param { string } name
   * @param { string } email
   * @param { string } id
   * @returns { ClientEntity }
   */
  static create(name, email, id = null) {
    return new ClientEntity(name, email, id)
  }
}
