import ProductEntity from '../domain/product/product.entity.js'

export default class ProductAdapter {
  /**
   *
   * @param { string } id
   * @param { string } title
   * @param { string } brand
   * @param { number } price
   * @param { string } image
   * @param { number } reviewScore
   * @returns { ProductEntity }
   */
  static create(id, title, brand, price, image, reviewScore = null) {
    return new ProductEntity(id, title, brand, price, image, reviewScore)
  }
}
