import UserEntity from '../domain/user/user.entity.js'

export default class UserAdapter {
  /**
   *
   * @param { string } email
   * @param { string } password
   * @param { string } id
   * @returns { UserEntity }
   */
  static create(email, password, id) {
    return new UserEntity(email, password, id)
  }
}
