import ProductRepositoryAxios from '../../../src/infra/repositories/product.repository.axios.js'
import FindProductUsecase from '../../../src/domain/product/find-product.usecase.js'
import {
  InvalidUuidError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import { ProductNotFoundById } from '../../../src/domain/product/product.error.js'

describe('Find product Use Case', () => {
  let findUseCase

  beforeEach(() => {
    const productRepository = new ProductRepositoryAxios()
    findUseCase = new FindProductUsecase(productRepository)
  })

  test('Success, find a product by id', async () => {
    const productId = '1bf0f365-fbdd-4e21-9786-da459d78dd1f'
    const productFound = await findUseCase.execute(productId)

    expect(productFound).toBeDefined()
    expect(productFound.id).toBe(productId)
    expect(productFound.title).not.toBeNull()
    expect(productFound.brand).not.toBeNull()
    expect(productFound.price).not.toBeNull()
    expect(productFound.image).not.toBeNull()
  })

  test('Fail, find by empty id', () => {
    const productFound = async () => {
      await findUseCase.execute('')
    }

    expect(productFound).rejects.toThrow(new RequiredFieldError('id'))
  })

  test('Fail, find by invalid uuid', () => {
    const invalidId = 'asdf123'
    const productFound = async () => {
      await findUseCase.execute(invalidId)
    }

    expect(productFound).rejects.toThrow(new InvalidUuidError('id'))
  })

  test('Fail, find by nonexistent id', () => {
    const nonExistentId = 'ba16c104-2495-401b-af45-697c6e31fc95'
    const productFound = async () => {
      await findUseCase.execute(nonExistentId)
    }

    expect(productFound).rejects.toThrow(new ProductNotFoundById(nonExistentId))
  })
})
