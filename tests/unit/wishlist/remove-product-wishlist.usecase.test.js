import AddProductWishlistUsecase from '../../../src/domain/wishlist/add-product-wishlist.usecase.js'
import ProductRepositoryAxios from '../../../src/infra/repositories/product.repository.axios.js'
import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import { WishlistProductNotExist } from '../../../src/domain/wishlist/wishlist.error.js'
import { ClientNotFoundById } from '../../../src/domain/client/client.error.js'
import RemoveProductWishlistUsecase from '../../../src/domain/wishlist/remove-product-wishlist.usecase.js'
import GetClientWishlistUsecase from '../../../src/domain/wishlist/get-client-wishlist.usecase.js'

describe('Remove product wishlist Use Case', () => {
  const wishInput = {}
  const validProductUuid = '1bf0f365-fbdd-4e21-9786-da459d78dd1f'
  const clientInput = {
    name: 'José da Silva',
    email: 'jose@hotmail.com',
  }
  let addProductWishlistUseCase
  let removeProductWishlistUseCase
  let getClientWishlistUseCase

  beforeEach(async () => {
    const productRepository = new ProductRepositoryAxios()
    const clientRepository = new ClientRepositoryMemory()

    const addClientUseCase = new CreateClientUsecase(clientRepository)
    const client = await addClientUseCase.execute(
      clientInput.name,
      clientInput.email
    )
    wishInput.clientId = client.id
    wishInput.productId = validProductUuid

    addProductWishlistUseCase = new AddProductWishlistUsecase(
      clientRepository,
      productRepository
    )
    removeProductWishlistUseCase = new RemoveProductWishlistUsecase(
      clientRepository,
      productRepository
    )
    getClientWishlistUseCase = new GetClientWishlistUsecase(clientRepository)
  })

  test("Success, remove a product after adding one to the client's wishlist", async () => {
    const product = await addProductWishlistUseCase.execute(
      wishInput.clientId,
      wishInput.productId
    )

    expect(product).toBeDefined()
    expect(product.id).toEqual(wishInput.productId)

    await removeProductWishlistUseCase.execute(
      wishInput.clientId,
      wishInput.productId
    )

    const wishListFound = await getClientWishlistUseCase.execute(
      wishInput.clientId
    )

    expect(wishListFound).toEqual([])
  })

  test('Fail, remove a new product to the wishlist of a non existent client ', () => {
    const nonExistentClientId = 'c2afe78f-8ba3-423c-b2d6-4452146ea48c'
    const removedWish = async () => {
      await removeProductWishlistUseCase.execute(
        nonExistentClientId,
        wishInput.productId
      )
    }

    expect(removedWish).rejects.toThrow(
      new ClientNotFoundById(nonExistentClientId)
    )
  })

  test('Fail, removed a non existent product to the client wishlist', () => {
    const nonExistentProductId = 'd526d88b-a9d9-4439-b0a0-a5da6906f71d'
    const removedWish = async () => {
      await removeProductWishlistUseCase.execute(
        wishInput.clientId,
        nonExistentProductId
      )
    }

    expect(removedWish).rejects.toThrow(
      new WishlistProductNotExist(nonExistentProductId)
    )
  })
})
