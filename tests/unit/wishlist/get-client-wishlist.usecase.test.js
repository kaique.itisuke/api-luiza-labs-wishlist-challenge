import AddProductWishlistUsecase from '../../../src/domain/wishlist/add-product-wishlist.usecase.js'
import ProductRepositoryAxios from '../../../src/infra/repositories/product.repository.axios.js'
import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import GetClientWishlistUsecase from '../../../src/domain/wishlist/get-client-wishlist.usecase.js'
import { ClientNotFoundById } from '../../../src/domain/client/client.error.js'

describe('Get client wishlist Use Case', () => {
  const wishInput = {}
  const validProductUuid = '1bf0f365-fbdd-4e21-9786-da459d78dd1f'
  const clientInput = {
    name: 'José da Silva',
    email: 'jose@hotmail.com',
  }
  let addProductWishlistUseCase
  let getClientWishlistUseCase

  beforeEach(async () => {
    const productRepository = new ProductRepositoryAxios()
    const clientRepository = new ClientRepositoryMemory()

    const addClientUseCase = new CreateClientUsecase(clientRepository)
    const client = await addClientUseCase.execute(
      clientInput.name,
      clientInput.email
    )
    wishInput.clientId = client.id
    wishInput.productId = validProductUuid

    addProductWishlistUseCase = new AddProductWishlistUsecase(
      clientRepository,
      productRepository
    )
    getClientWishlistUseCase = new GetClientWishlistUsecase(clientRepository)
  })

  test('Success, get wishlist after adding a new product to the list', async () => {
    await addProductWishlistUseCase.execute(
      wishInput.clientId,
      wishInput.productId
    )
    const wishList = await getClientWishlistUseCase.execute(wishInput.clientId)

    expect(wishList.length).toEqual(1)
    expect(wishList[0].id).toEqual(wishInput.productId)
  })

  test('Success, get empty wishlist', async () => {
    const wishList = await getClientWishlistUseCase.execute(wishInput.clientId)

    expect(wishList).toEqual([])
  })

  test('Fail, get list of non existent client', () => {
    const nonExistentClientUuid = '62df7ef8-8037-4eb5-acc4-4edd23dbd4f5'
    const wishList = async () => {
      await getClientWishlistUseCase.execute(nonExistentClientUuid)
    }

    expect(wishList).rejects.toThrow(
      new ClientNotFoundById(nonExistentClientUuid)
    )
  })
})
