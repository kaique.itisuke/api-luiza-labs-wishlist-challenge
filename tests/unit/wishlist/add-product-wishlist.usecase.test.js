import AddProductWishlistUsecase from '../../../src/domain/wishlist/add-product-wishlist.usecase.js'
import ProductRepositoryAxios from '../../../src/infra/repositories/product.repository.axios.js'
import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import { WishlistProductAlreadyAdded } from '../../../src/domain/wishlist/wishlist.error.js'
import { ClientNotFoundById } from '../../../src/domain/client/client.error.js'
import { ProductNotFoundById } from '../../../src/domain/product/product.error.js'

describe('Add product wishlist Use Case', () => {
  const wishInput = {}
  const validProductUuid = '1bf0f365-fbdd-4e21-9786-da459d78dd1f'
  const clientInput = {
    name: 'José da Silva',
    email: 'jose@hotmail.com',
  }
  let addProductWishlistUseCase

  beforeEach(async () => {
    const productRepository = new ProductRepositoryAxios()
    const clientRepository = new ClientRepositoryMemory()

    const addClientUseCase = new CreateClientUsecase(clientRepository)
    const client = await addClientUseCase.execute(
      clientInput.name,
      clientInput.email
    )
    wishInput.clientId = client.id
    wishInput.productId = validProductUuid

    addProductWishlistUseCase = new AddProductWishlistUsecase(
      clientRepository,
      productRepository
    )
  })

  test('Success, add a new product to the client wishlist', async () => {
    const product = await addProductWishlistUseCase.execute(
      wishInput.clientId,
      wishInput.productId
    )

    expect(product).toBeDefined()
    expect(product.id).toEqual(wishInput.productId)
  })

  test('Fail, add a new product to the wishlist of a non existent client ', async () => {
    const nonExistentClientId = 'c2afe78f-8ba3-423c-b2d6-4452146ea48c'
    const addedWish = async () => {
      await addProductWishlistUseCase.execute(
        nonExistentClientId,
        wishInput.productId
      )
    }

    expect(addedWish).rejects.toThrow(
      new ClientNotFoundById(nonExistentClientId)
    )
  })

  test('Fail, add a non existent product to the client wishlist', () => {
    const nonExistentProductId = 'd526d88b-a9d9-4439-b0a0-a5da6906f71d'
    const addedWish = async () => {
      await addProductWishlistUseCase.execute(
        wishInput.clientId,
        nonExistentProductId
      )
    }

    expect(addedWish).rejects.toThrow(
      new ProductNotFoundById(nonExistentProductId)
    )
  })

  test('Fail, add a product that already exist on the wishlist', async () => {
    await addProductWishlistUseCase.execute(
      wishInput.clientId,
      wishInput.productId
    )
    const secondAddedWish = async () => {
      await addProductWishlistUseCase.execute(
        wishInput.clientId,
        wishInput.productId
      )
    }

    expect(secondAddedWish).rejects.toThrow(
      new WishlistProductAlreadyAdded(wishInput.productId)
    )
  })
})
