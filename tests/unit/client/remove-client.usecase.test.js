import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import { ClientNotFoundById } from '../../../src/domain/client/client.error.js'
import {
  InvalidUuidError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import RemoveClientUsecase from '../../../src/domain/client/remove-client.usecase.js'
import FindClientUsecase from '../../../src/domain/client/find-client.usecase.js'

describe('Remove client Use Case', () => {
  let createUseCase
  let removeUseCase
  let findUseCase

  beforeEach(() => {
    const clientRepository = new ClientRepositoryMemory()
    createUseCase = new CreateClientUsecase(clientRepository)
    removeUseCase = new RemoveClientUsecase(clientRepository)
    findUseCase = new FindClientUsecase(clientRepository)
  })

  test('Success, add a new client and then remove it by id', async () => {
    const clientInput = {
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }

    const createdClient = await createUseCase.execute(
      clientInput.name,
      clientInput.email
    )

    expect(createdClient.id).not.toBeNull()
    expect(createdClient.name).toBe(clientInput.name)
    expect(createdClient.email).toBe(clientInput.email)

    await removeUseCase.execute(createdClient.id)

    const clientFound = async () => {
      await findUseCase.execute(createdClient.id)
    }

    expect(clientFound).rejects.toThrow(
      new ClientNotFoundById(createdClient.id)
    )
  })

  test('Fail, remove by empty id', () => {
    const clientFound = async () => {
      await removeUseCase.execute('')
    }

    expect(clientFound).rejects.toThrow(new RequiredFieldError('id'))
  })

  test('Fail, remove by invalid uuid', () => {
    const invalidId = 'asdf123'
    const clientFound = async () => {
      await removeUseCase.execute(invalidId)
    }

    expect(clientFound).rejects.toThrow(new InvalidUuidError('id'))
  })

  test('Fail, remove by nonexistent id', () => {
    const nonExistentId = 'ba16c104-2495-401b-af45-697c6e31fc95'
    const clientFound = async () => {
      await removeUseCase.execute(nonExistentId)
    }

    expect(clientFound).rejects.toThrow(new ClientNotFoundById(nonExistentId))
  })
})
