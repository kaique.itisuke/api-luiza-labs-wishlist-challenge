import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import GetClientUsecase from '../../../src/domain/client/get-client.usecase.js'

describe('Get client Use Case', () => {
  let createUseCase
  let getUseCase

  beforeEach(() => {
    const clientRepository = new ClientRepositoryMemory()
    createUseCase = new CreateClientUsecase(clientRepository)
    getUseCase = new GetClientUsecase(clientRepository)
  })

  test('Success, register new clients and then gets a list of results', async () => {
    const names = ['Jose', 'Joao', 'Josue', 'Joana', 'Joaquina']
    const createdClients = []

    for (const name of names) {
      const client = await createUseCase.execute(
        `${name} da Silva`,
        `${name}@hotmail.com`
      )
      createdClients.push(client)
    }

    const clientsFound = await getUseCase.execute()

    clientsFound.forEach((clientFound) => {
      const createdClient = createdClients.find(
        (client) => client.id === clientFound.id
      )

      expect(clientFound).toEqual(createdClient)
    })
  })

  test('Success, empty list of clients', async () => {
    const clients = await getUseCase.execute()

    expect(clients).toEqual([])
  })
})
