import { ClientEmailAlreadyInUse } from '../../../src/domain/client/client.error.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import {
  InvalidFieldError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'

describe('Create client Use Case', () => {
  let createUseCase

  beforeEach(() => {
    const clientRepository = new ClientRepositoryMemory()
    createUseCase = new CreateClientUsecase(clientRepository)
  })

  test('Success, add a new client', async () => {
    const clientInput = {
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }

    const createdClient = await createUseCase.execute(
      clientInput.name,
      clientInput.email
    )

    expect(createdClient.id).not.toBeNull()
    expect(createdClient.name).toBe(clientInput.name)
    expect(createdClient.email).toBe(clientInput.email)
  })

  test('Fail, add a new client without name', () => {
    const clientInput = {
      name: '',
      email: 'jose@hotmail.com',
    }

    const createdClient = async () => {
      await createUseCase.execute(clientInput.name, clientInput.email)
    }

    expect(createdClient).rejects.toThrow(new RequiredFieldError('name'))
  })

  test('Fail, add a new client without email', () => {
    const clientInput = {
      name: 'Joaquim Soares',
      email: '',
    }

    const createdClient = async () => {
      await createUseCase.execute(clientInput.name, clientInput.email)
    }

    expect(createdClient).rejects.toThrow(new RequiredFieldError('email'))
  })

  test('Fail, add a new client with invalid email', () => {
    const clientInput = {
      name: 'Maria Duarte',
      email: 'maria.com.br',
    }

    const createdClient = async () => {
      await createUseCase.execute(clientInput.name, clientInput.email)
    }

    expect(createdClient).rejects.toThrow(new InvalidFieldError('email'))
  })

  test('Fail, add a new client with an existing email', async () => {
    const firstClientInput = {
      name: 'José Antunes',
      email: 'j_antunes@hotmail.com',
    }
    const secondClientInput = {
      name: 'João Antunes',
      email: 'j_antunes@hotmail.com',
    }

    await createUseCase.execute(firstClientInput.name, firstClientInput.email)

    const createdSecondClient = async () => {
      await createUseCase.execute(
        secondClientInput.name,
        secondClientInput.email
      )
    }

    expect(createdSecondClient).rejects.toThrow(
      new ClientEmailAlreadyInUse(secondClientInput.email)
    )
  })
})
