import {
  ClientEmailAlreadyInUse,
  ClientNotFoundById,
} from '../../../src/domain/client/client.error.js'
import CreateClientUsecase from '../../../src/domain/client/create-client.usecase.js'
import UpdateClientUsecase from '../../../src/domain/client/update-client.usecase.js'
import {
  InvalidFieldError,
  InvalidUuidError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import ClientRepositoryMemory from '../../../src/infra/repositories/client.repository.memory.js'

describe('Update client Use Case', () => {
  let createUseCase
  let createdClient
  let updateUseCase

  beforeEach(async () => {
    const clientRepository = new ClientRepositoryMemory()
    createUseCase = new CreateClientUsecase(clientRepository)

    const clientInput = {
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }

    createdClient = await createUseCase.execute(
      clientInput.name,
      clientInput.email
    )

    updateUseCase = new UpdateClientUsecase(clientRepository)
  })

  test('Success, update a client after creating one', async () => {
    const clientInput = {
      id: createdClient.id,
      name: 'Joana Soares',
      email: 'joana_srs@hotmail.com',
    }

    const updatedClient = await updateUseCase.execute(
      clientInput.id,
      clientInput.name,
      clientInput.email
    )

    expect(clientInput).toEqual(updatedClient)
  })

  test('Fail, update by empty id', () => {
    let clientInput = {
      id: '',
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }
    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new RequiredFieldError('id'))
  })

  test('Fail, update by invalid uuid', () => {
    const invalidId = 'asdf123'
    let clientInput = {
      id: invalidId,
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }
    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new InvalidUuidError('id'))
  })

  test('Fail, find by nonexistent id', () => {
    const nonExistentId = 'ba16c104-2495-401b-af45-697c6e31fc95'
    let clientInput = {
      id: nonExistentId,
      name: 'Jose da Silva',
      email: 'jose@hotmail.com',
    }
    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new ClientNotFoundById(nonExistentId))
  })

  test('Fail, add a new client without name', () => {
    const clientInput = {
      id: createdClient.id,
      name: '',
      email: 'joana_srs@hotmail.com',
    }

    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new RequiredFieldError('name'))
  })

  test('Fail, add a new client without email', () => {
    const clientInput = {
      id: createdClient.id,
      name: 'Jose da Silva',
      email: '',
    }

    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new RequiredFieldError('email'))
  })

  test('Fail, add a new client without email', () => {
    const clientInput = {
      id: createdClient.id,
      name: 'Jose da Silva',
      email: 'jose.com.br',
    }

    const updatedClient = async () => {
      await updateUseCase.execute(
        clientInput.id,
        clientInput.name,
        clientInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new InvalidFieldError('email'))
  })

  test('Fail, update an email that is already used by another client', async () => {
    const newEmail = 'j_antunes@hotmail.com'
    const secondClientInput = {
      name: 'João Antunes',
      email: newEmail,
    }

    const secondClientCreated = await createUseCase.execute(
      secondClientInput.name,
      secondClientInput.email
    )

    const clientUpdateInput = {
      id: createdClient.id,
      name: 'Joana Soares',
      email: secondClientCreated.email,
    }

    const updatedClient = async () => {
      await updateUseCase.execute(
        clientUpdateInput.id,
        clientUpdateInput.name,
        clientUpdateInput.email
      )
    }

    expect(updatedClient).rejects.toThrow(new ClientEmailAlreadyInUse(newEmail))
  })
})
