import { UserEmailAlreadyInUse } from '../../../src/domain/user/user.error.js'
import CreateUserUsecase from '../../../src/domain/user/create-user.usecase.js'
import {
  EqualFieldsError,
  InvalidFieldError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import UserRepositoryMemory from '../../../src/infra/repositories/user.repository.memory.js'
import PasswordBcryptEncrypter from '../../../src/infra/encrpyters/password.bcrypt.encrypter.js'
import JwtAuthenticator from '../../../src/infra/authentication/jwt.authenticator.js'

describe('Create user Use Case', () => {
  let createUseCase

  beforeEach(() => {
    const userRepository = new UserRepositoryMemory()
    const passwordEncrypter = new PasswordBcryptEncrypter()
    const tokenAuthenticator = new JwtAuthenticator()
    createUseCase = new CreateUserUsecase(
      userRepository,
      passwordEncrypter,
      tokenAuthenticator
    )
  })

  test('Success, add a new user', async () => {
    const userInput = {
      email: 'jose@hotmail.com',
      password: 'jose123456',
      passwordConfirmation: 'jose123456',
    }

    const createdUser = await createUseCase.execute(
      userInput.email,
      userInput.password,
      userInput.passwordConfirmation
    )

    expect(createdUser.id).toBeDefined()
    expect(createdUser.email).toBe(userInput.email)
    expect(createdUser.token).toBeDefined()
  })

  test('Fail, add a new user without email', () => {
    const userInput = {
      email: '',
      password: '123456',
    }

    const createdUser = async () => {
      await createUseCase.execute(userInput.email, userInput.password)
    }

    expect(createdUser).rejects.toThrow(new RequiredFieldError('email'))
  })

  test('Fail, add a new user without password', () => {
    const userInput = {
      email: 'jhon@hotmail.com',
      password: '',
    }

    const createdUser = async () => {
      await createUseCase.execute(userInput.email, userInput.password)
    }

    expect(createdUser).rejects.toThrow(new RequiredFieldError('password'))
  })

  test('Fail, add a new user with invalid email', () => {
    const userInput = {
      email: 'mario.com',
      password: 'Mario12312452*',
      passwordConfirmation: 'Mario12312452*',
    }

    const createdUser = async () => {
      await createUseCase.execute(
        userInput.email,
        userInput.password,
        userInput.passwordConfirmation
      )
    }

    expect(createdUser).rejects.toThrow(new InvalidFieldError('email'))
  })

  test('Fail, add a new user with an invalid password confirmation', () => {
    const userInput = {
      email: 'josefina@hotmail.com',
      password: 'VALID_PASSWORD',
      passwordConfirmation: 'INVALID_PASSWORD_CONFIRMATION',
    }

    const createdUser = async () => {
      await createUseCase.execute(
        userInput.email,
        userInput.password,
        userInput.passwordConfirmation
      )
    }

    expect(createdUser).rejects.toThrow(
      new EqualFieldsError('passwordConfirmation', 'password')
    )
  })

  test('Fail, add a new user with an existing email', async () => {
    const firstUserInput = {
      email: 'j_antunes@hotmail.com',
      password: '123456',
      passwordConfirmation: '123456',
    }
    const secondUserInput = {
      email: 'j_antunes@hotmail.com',
      password: '123456',
      passwordConfirmation: '123456',
    }

    await createUseCase.execute(
      firstUserInput.email,
      firstUserInput.password,
      firstUserInput.passwordConfirmation
    )

    const createdSecondUser = async () => {
      await createUseCase.execute(
        secondUserInput.email,
        secondUserInput.password,
        secondUserInput.passwordConfirmation
      )
    }

    expect(createdSecondUser).rejects.toThrow(
      new UserEmailAlreadyInUse(secondUserInput.email)
    )
  })
})
