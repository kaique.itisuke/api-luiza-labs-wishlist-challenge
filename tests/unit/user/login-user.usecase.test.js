import {
  UserInvalidPassword,
  UserNotFoundByEmail,
} from '../../../src/domain/user/user.error.js'
import CreateUserUsecase from '../../../src/domain/user/create-user.usecase.js'
import {
  InvalidFieldError,
  RequiredFieldError,
} from '../../../src/helpers/errors/custom.error.js'
import UserRepositoryMemory from '../../../src/infra/repositories/user.repository.memory.js'
import PasswordBcryptEncrypter from '../../../src/infra/encrpyters/password.bcrypt.encrypter.js'
import JwtAuthenticator from '../../../src/infra/authentication/jwt.authenticator.js'
import LoginUserUsecase from '../../../src/domain/user/login-user.usecase.js'

describe('Create user Use Case', () => {
  let createUseCase
  let loginUseCase

  beforeEach(() => {
    const userRepository = new UserRepositoryMemory()
    const passwordEncrypter = new PasswordBcryptEncrypter()
    const tokenAuthenticator = new JwtAuthenticator()
    createUseCase = new CreateUserUsecase(
      userRepository,
      passwordEncrypter,
      tokenAuthenticator
    )
    loginUseCase = new LoginUserUsecase(
      userRepository,
      passwordEncrypter,
      tokenAuthenticator
    )
  })

  test('Success, add a new user and try to log in', async () => {
    const userInput = {
      email: 'ALID_EMAIL@hotmail.com',
      password: 'VALID_PASSWORD',
      passwordConfirmation: 'VALID_PASSWORD',
    }

    const createdUser = await createUseCase.execute(
      userInput.email,
      userInput.password,
      userInput.passwordConfirmation
    )

    expect(createdUser.id).not.toBeNull()
    expect(createdUser.email).toBe(userInput.email)
    expect(createdUser.password).not.toBe(userInput.password)

    const loggedUser = await loginUseCase.execute(
      userInput.email,
      userInput.password
    )

    expect(loggedUser).toBeDefined()
    expect(loggedUser.token).toBeDefined()
  })

  test('Fail, try to log in with a valid email and an incorrect password', async () => {
    const userInput = {
      email: 'VALID_EMAIL@hotmail.com',
      password: 'VALID_PASSWORD',
      passwordConfirmation: 'VALID_PASSWORD',
    }

    const createdUser = await createUseCase.execute(
      userInput.email,
      userInput.password,
      userInput.passwordConfirmation
    )

    expect(createdUser.id).not.toBeNull()
    expect(createdUser.email).toBe(userInput.email)
    expect(createdUser.password).not.toBe(userInput.password)

    const invalidPassword = 'INVALID_PASSWORD'
    const loggedUser = async () => {
      await loginUseCase.execute(userInput.email, invalidPassword)
    }

    expect(loggedUser).rejects.toThrow(new UserInvalidPassword())
  })

  test('Fail, try to log in without email', () => {
    const userInput = {
      email: '',
      password: '123456',
    }

    const loggedUser = async () => {
      await loginUseCase.execute(userInput.email, userInput.password)
    }

    expect(loggedUser).rejects.toThrow(new RequiredFieldError('email'))
  })

  test('Fail, try to log in without password', () => {
    const userInput = {
      email: 'jhon@hotmail.com',
      password: '',
    }

    const loggedUser = async () => {
      await loginUseCase.execute(userInput.email, userInput.password)
    }

    expect(loggedUser).rejects.toThrow(new RequiredFieldError('password'))
  })

  test('Fail, try to log in with an invalid email', () => {
    const userInput = {
      email: 'INVALID_EMAIL.COM',
      password: 'VALID_PASSWORD',
    }

    const loggedUser = async () => {
      await loginUseCase.execute(userInput.email, userInput.password)
    }

    expect(loggedUser).rejects.toThrow(new InvalidFieldError('email'))
  })

  test('Fail, try to log in with an non existent email', () => {
    const userInput = {
      email: 'VALID_EMAIL@hotmail.com',
      password: 'VALID_PASSWORD',
    }

    const loggedUser = async () => {
      await loginUseCase.execute(userInput.email, userInput.password)
    }

    expect(loggedUser).rejects.toThrow(new UserNotFoundByEmail(userInput.email))
  })
})
